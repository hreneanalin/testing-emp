package AngajatiApp.repository;

import AngajatiApp.model.DidacticFunction;
import AngajatiApp.model.Employee;
import AngajatiApp.model.exceptions.InvalidEmployeeException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeMockTest {
    //lab 03
    EmployeeMock employeeRepository = new EmployeeMock();
    Employee employee = new Employee("Ion", "Popescu", "1950505313707", DidacticFunction.ASISTENT, 2000.0);
    Employee employee1 = new Employee("", "Popescu", "1950505313707", DidacticFunction.ASISTENT, 2000.0);
    Employee employee2 = new Employee("Ion", "", "1950505313707", DidacticFunction.ASISTENT, 2000.0);
    Employee employee3 = new Employee("Po", "Popescu", "1950518313707", DidacticFunction.ASISTENT, 2000.0);
    Employee employee4 = new Employee("Pop", "Po", "1950505313707", DidacticFunction.ASISTENT, 2000.0);


    @Test
    void addValidEmployee() {
        int number = employeeRepository.getEmployeeList().size();
        employeeRepository.addEmployee(employee);
        assertEquals(number + 1, employeeRepository.getEmployeeList().size());
    }

    @Test
    void addInValidEmployee2() {

        assertThrows(InvalidEmployeeException.class, () -> {
            employeeRepository.addEmployee(employee3);

        });
    }


    @Test
    void addEmployeeWithInvalidFirstName() {
        assertThrows(InvalidEmployeeException.class, () -> {
            employeeRepository.addEmployee(employee1);

        });
    }

    @Test
    void addEmployeeWithInvaliFirstName() {
        assertThrows(InvalidEmployeeException.class, () -> {
            employeeRepository.addEmployee(employee3);

        });
    }

    @Test
    void addEmployeeWithInvaliLastName() {
        assertThrows(InvalidEmployeeException.class, () -> {
            employeeRepository.addEmployee(employee4);

        });
    }

    @Test
    void addEmployeeWithInvaliLastName2() {
        assertThrows(InvalidEmployeeException.class, () -> {
            employeeRepository.addEmployee(employee2);

        });
    }

    //lab 04
    @Test
    void modifyNullEmployeeFunction() {
        assertAll(() -> {
            employeeRepository.modifyEmployeeFunction(null, DidacticFunction.ASISTENT);
        });
    }

    @Test
    void modifyEmployeeFunctionFromEmptyRepository() {

        EmployeeMock employeeMockEmpty = new EmployeeMock();
        employeeMockEmpty.modifyEmployeeFunction(employee1, DidacticFunction.TEACHER);
        assertNotEquals(employee1.getFunction(), DidacticFunction.TEACHER);

    }

    @Test
    void modifyEmployeeFunction(){
        Employee someEmployee = new Employee("Ion", "Popescu", "1950505313707", DidacticFunction.ASISTENT, 2000.0);
        employeeRepository.addEmployee(someEmployee);
        employeeRepository.modifyEmployeeFunction(someEmployee,DidacticFunction.TEACHER);
        assertEquals(someEmployee.getFunction(),DidacticFunction.TEACHER);
    }



}